﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Logging;

namespace WeavingProdControl.DataAccess.Machine
{
    public class MachineDataDao : TimeRelevant
    {
        private static MachineDataDao instance;

        public static MachineDataDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MachineDataDao();
                return instance;
            }
        }
        private List<Telare> machines;
        private object machineLock;
        private MachineDataDao()
        {
            this.machineLock = new object();
            this.machines = null;
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.MachineDataRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.machines == null);
            }
        }

        public List<Telare> Machines
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new CPTelaresEntities())
                    {
                        var requestQuery = from pauta in Context.Telares
                                           select pauta;
                        try
                        {
                            lock (this.machineLock)
                            {
                                this.machines = requestQuery.ToList<Telare>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.machineLock)
                {
                    if (this.machines != null)
                        return new List<Telare>(this.machines);
                    return new List<Telare>();
                }
            }
        }

    }
}
