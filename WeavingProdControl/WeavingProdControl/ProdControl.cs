﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Service;

namespace WeavingProdControl
{
    public partial class ProdControl : ServiceBase
    {
        private ServiceProcess process;
        public ProdControl()
        {
            this.process = new ServiceProcess();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.process.Start();
        }

        protected override void OnStop()
        {
            this.process.Stop();
        }
    }
}
