﻿using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Logging;

namespace WeavingProdControl.DataOp
{
    public class MachineSaveData
    {
        public AdamConnector.SaveData Saver;
        public AdamConnector.ErrorLog Err;
        private int machineId;
        public MachineSaveData(int machineId)
        {
            this.Saver = SaveData;
            this.Err = ErrorLog;
            this.machineId = machineId;
        }

        private bool SaveData(AdamData davaValues)
        {
            return false;
        }

        private void ErrorLog(string errorMsg)
        {
            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), machineId + " " + errorMsg);
        }
    }
}
