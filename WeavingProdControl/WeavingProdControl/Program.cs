﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.Service;

namespace WeavingProdControl
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            ServiceProcess proc = new ServiceProcess();
            proc.Start();
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ProdControl()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
