﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WeavingProdControl.Service
{
    public abstract class Process
    {
        private int loopRepeatTime;
        private Thread proc;
        private DateTime startTime;
        private bool running;
        private bool noRepeat;
        public Process()
        {
            this.noRepeat = true;
        }
        public Process(int loopRepeatTime)
        {
            this.noRepeat = false;
            this.loopRepeatTime = loopRepeatTime;
        }

        private void Go()
        {
            if (this.noRepeat)
            {
                Run();
            }
            else
            {
                while (this.running)
                {
                    DateTime startTime = DateTime.Now;
                    Run();
                    Sleep(this.startTime, this.loopRepeatTime);
                }
            }
        }

        private int GetSleepTimeAmount(DateTime startTime, int loopRepeatTime)
        {
            TimeSpan difference = DateTime.Now - startTime;
            if (difference.TotalMilliseconds >= loopRepeatTime)
                return 0;
            else
                return loopRepeatTime - (int)difference.TotalMilliseconds;
        }

        private void Sleep(DateTime startTime, int loopRepeatTime)
        {
            while (GetSleepTimeAmount(startTime, loopRepeatTime) > 0 && this.running)
            {
                Thread.Sleep(Properties.Settings.Default.StopSleepTimeOut);
            }
        }

        public bool Start()
        {
            this.running = true;
            this.proc = new Thread(Go);
            this.proc.Start();
            return true;
        }

        public bool Stop()
        {
            if (this.noRepeat)
            {
                return StopNonRepeat();
            }
            else
            {
                this.running = false;
                return true;
            }
        }

        protected abstract void Run();
        protected abstract bool StopNonRepeat();
    }
}
