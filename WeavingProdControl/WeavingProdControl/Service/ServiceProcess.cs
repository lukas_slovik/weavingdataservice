﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeavingProdControl.Service
{
    public class ServiceProcess
    {
        private List<Process> processes;
        public ServiceProcess()
        {
            this.processes = new List<Process>();
            this.processes.Add(new IoDataReaderProcess());
        }
        public bool Start()
        {
            bool result = true;
            foreach (Process proc in this.processes)
            {
                result &= proc.Start();
            }
            return result;
        }

        public bool Stop()
        {
            bool result = true;
            foreach (Process proc in this.processes)
            {
                result &= proc.Stop();
            }
            return result;
        }
    }
}
