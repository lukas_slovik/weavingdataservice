﻿using AdamUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeavingProdControl.DataAccess;
using WeavingProdControl.DataAccess.Machine;
using WeavingProdControl.DataOp;
using WeavingProdControl.Logging;

namespace WeavingProdControl.Service
{
    public class IoDataReaderProcess : Process
    {
        private List<AdamConnector> adamConnectors;
        private List<MachineSaveData> savers;
        public IoDataReaderProcess() : base()
        {
            this.adamConnectors = new List<AdamConnector>();
            this.savers = new List<MachineSaveData>();
        }

        private void LoadMachines()
        {
            List<Telare> machines = MachineDataDao.Instance.Machines;
            foreach (Telare machine in machines)
            {
                if (machine.Activo != null)
                {
                    if ((bool)machine.Activo)
                    {
                        MachineSaveData saver = new MachineSaveData(machine.idTelar);
                        this.savers.Add(saver);
                        this.adamConnectors.Add(new AdamConnector(machine.DireccionIP.Trim(),
                            Properties.Settings.Default.MachineConnectionDefaultPort,
                            Properties.Settings.Default.MachineDisconnectTimeOut,
                            Properties.Settings.Default.MachineReadTimeOut,
                            saver.Saver,
                            saver.Err,
                            AdamDataReadout.ReadCounters));
                    }
                }
            }
        }

        private void StartDataCollection()
        {
            foreach (AdamConnector machinReader in this.adamConnectors)
            {
                machinReader.Start();
            }
        }

        protected override void Run()
        {
            LoadMachines();
            StartDataCollection();
        }

        protected override bool StopNonRepeat()
        {
            foreach (AdamConnector machinReader in this.adamConnectors)
            {
                machinReader.Stop();
            }
            return true;
        }
    }
}
